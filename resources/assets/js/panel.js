$(document).ready(function(){
   console.log('ready');

    $(".checkTitle").keyup(function(e){
        if($(".checkTitle").val() != title) {
            var data = {title: $(".checkTitle").val(), api_token: api_token};
            axios.post('/api/panel/checkTitle', data)
                .then(response => {
                    if (response.data == true) {
                        $(".checkTitle").css('background-color', 'rgba(193,66,66,0.8)');
                        $("#error").text('Titel bestaal al');
                    } else {
                        $(".checkTitle").css('background-color', '#fff');
                        $("#error").text('');
                    }
                    console.log(response);
                })
        }
    });

});
