<nav class="m-l-15 m-t-15">
    <a href="{{ route('page.index') }}">Home</a>
    @guest
        <a class="m-r-15" style="float:right;" href="{{ route('login') }}">Login</a>
    @endguest
    @auth
        <a class="m-r-15" style="float:right;" href="{{ route('logout') }}">Uitloggen</a>
        <a class="m-r-15" style="float: right;" href="{{ route('page.create') }}">Maak nieuwe blog</a>
        <a class="m-r-15" style="float: right;" href="{{ route('user.create') }}">Maak nieuwe gebruiker</a>
        <a class="m-r-15" style="float: right;" href="{{ route('user.index') }}">Gebruikers</a>
    @endauth
</nav>