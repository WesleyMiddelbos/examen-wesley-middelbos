@extends('layouts.app')

@section('title','ExamenBlog - Gebruikers overzicht')

@section('content')
    @foreach($users as $user)
        <div class="post m-l-15 m-t-15 m-r-15">
            <h1>{{ $user->name }}</h1>
            <h3>{{ $user->email }}</h3>
            <p>Gemaakt op: {{ $user->created_at->format('d-m-Y') }}</p>
            @auth
                <p><a href="{{ route('user.edit',$user->id) }}">Bewerk gebruiker</a></p>
            @endauth
        </div>
    @endforeach
@endsection