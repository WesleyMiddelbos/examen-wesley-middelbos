@extends('layouts.app')

@section('title','ExamenBlog - Nieuw gebruiker')

@section('content')
    <div class="m-l-15 m-r-15">
        <h1>Nieuwe gebruiker</h1>
        <a href="{{ route('user.index') }}">Ga terug</a>
        <div class="panel p-t-15">
            @if(old('error'))
                <div class="alert alert-danger">{{ old('error') }}</div>
            @endif
            <form action="{{ route('user.store') }}" method="post">
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="name">Naam</label>
                        <input class="form-control" name="name" type="text" id="name" placeholder="Naam" value="{{ old('name') }}">
                        <p id="error"></p>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="email">Email</label>
                        <input class="form-control checkEmail" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="email">Wachtwoord</label>
                        <input class="form-control" type="password" name="password" placeholder="Wachtwoord" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <input style="float: right;" class="button" type="submit" value="Maak aan">
                    </div>
                </div>
                {{ @csrf_field() }}
            </form>

            <div class="clearfix"></div>
        </div>
    </div>
@endsection