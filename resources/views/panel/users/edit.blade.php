@extends('layouts.app')

@section('title','ExamenBlog - '.$user->name." bewerken")

@section('content')
    <div class="m-l-15 m-r-15">
        <h1>{{ $user->name }}</h1>
        <a href="{{ route('user.index') }}">Ga terug</a>
        <div class="panel p-t-15">
            @if(old('error'))
                <div class="alert alert-danger">{{ old('error') }}</div>
            @endif
            <form action="{{ route('user.update', $user->id) }}" method="post">
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="name">Naam</label>
                        <input class="form-control checkTitle" name="name" type="text" id="name" placeholder="Naam" value="{{ $user->name }}">
                        <p id="error"></p>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="email">Email</label>
                        <input class="form-control" type="email" name="email" id="email" value="{{ $user->email }}">
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" value="">
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <input style="float: right;" class="button" type="submit" value="Bewerk">
                    </div>
                </div>
                <input type="hidden" name="_method" value="patch">
                {{ @csrf_field() }}
            </form>
            <form action="{{ route('user.destroy',$user->id) }}" method="post">
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <input type="submit" style="float: right;" value="Verwijder">
                        {{ @csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    </div>
                </div>
            </form>

            <div class="clearfix"></div>
        </div>
    </div>
@endsection