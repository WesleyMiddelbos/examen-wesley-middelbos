@extends('layouts.app')

@section('title','ExamenBlog - Nieuw bericht')

@section('content')
    <div class="m-l-15 m-r-15">
        <h1>{{ $bericht->titel }}</h1>
        <a href="{{ route('page.index') }}">Ga terug</a>
        <div class="panel p-t-15">
            @if(old('error'))
                <div class="alert alert-danger">{{ old('error') }}</div>
            @endif
            <form action="{{ route('page.update', $bericht->id) }}" method="post">
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="title">Titel</label>
                        <input class="form-control checkTitle" name="titel" type="text" id="title" placeholder="titel" value="{{ $bericht->titel }}">
                        <p id="error"></p>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="content">Content</label>
                        <textarea name="content" name="content" class="form-control" id="content" cols="30" rows="10">{{ $bericht->content }}</textarea>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <input style="float: right;" class="button" type="submit" value="Bewerk">
                    </div>
                </div>
                <input type="hidden" name="_method" value="patch">
                {{ @csrf_field() }}
            </form>
                <form action="{{ route('page.destroy',$bericht->id) }}" method="post">
                    <div class="form-group col-lg-12 newRow">
                        <div class="col-lg-4 col-lg-push-4">
                            <input type="submit" style="float: right;" value="Verwijder">
                            {{ @csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                        </div>
                    </div>
                </form>

            <div class="clearfix"></div>
        </div>
    </div>
    <script>
        var title = "{{ $bericht->titel }}";
    </script>
@endsection