@extends('layouts.app')

@section('title','ExamenBlog - Nieuw bericht')

@section('content')
    <div class="m-l-15 m-r-15">
        <h1>Nieuwe pagina</h1>
        <a href="{{ route('page.index') }}">Ga terug</a>
        <div class="panel p-t-15">
            @if(old('error'))
                <div class="alert alert-danger">{{ old('error') }}</div>
            @endif
            <form action="{{ route('page.store') }}" method="post" enctype="multipart/form-data">
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="title">Titel</label>
                        <input class="form-control checkTitle" name="titel" type="text" id="title" placeholder="titel" value="{{ old('titel') }}">
                        <p id="error"></p>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="content">Content</label>
                        <textarea name="content" class="form-control" id="content" cols="30" rows="10">{{ old('content') }}</textarea>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <label class="control-label" for="images">Afbeeldingen</label>
                        <input name="header" class="form-control" id="images" type="file" accept="image/*"/>
                    </div>
                </div>
                <div class="form-group col-lg-12 newRow">
                    <div class="col-lg-4 col-lg-push-4">
                        <input style="float: right;" class="button" type="submit" value="Maak aan">
                    </div>
                </div>
                {{ @csrf_field() }}
            </form>

            <div class="clearfix"></div>
        </div>
    </div>
    <script>
        var title = "";
    </script>
@endsection