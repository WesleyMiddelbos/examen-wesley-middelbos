@extends('layouts.app')

@section('title','ExamenBlog - Overzicht')

@section('content')
    @foreach($berichten as $bericht)
        <div class="post m-l-15 m-t-15 m-r-15">
            <h1>{{ $bericht->titel }}</h1>
            <p>Gemaakt op: {{ $bericht->created_at->format('d-m-Y') }}</p>
            <p><a href="{{ route('page.show',$bericht->titel) }}">Bezoek pagina</a></p>
            @auth
                <p><a href="{{ route('page.edit',$bericht->id) }}">Bewerk pagina</a></p>
            @endauth
        </div>
    @endforeach
@endsection