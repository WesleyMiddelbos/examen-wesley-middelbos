@extends('layouts.app')

@section('title', "ExmamenBlog - ".$bericht->title)

@section('content')
    <div class="post m-l-15 m-t-15 m-r-15">
        @if(!is_null($bericht->header))
        <div class="col-lg-12">
            <img class="header" src="{{ route('page.header',$bericht->header->id) }}" alt="">
        </div>
        @endif
        <h1>{{ $bericht->titel }}</h1>
        <div class="m-t-15">
            <p>{{ $bericht->content }}</p>
        </div>
        <p>Gemaakt op: {{ $bericht->created_at->format('d-m-Y') }}</p>
        @auth
            <p><a href="{{ route('page.edit',$bericht->id) }}">Bewerk pagina</a></p>
        @endauth
    </div>
@endsection