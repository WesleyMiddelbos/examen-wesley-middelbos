<?php

namespace App\Http\Controllers;

use App\Bericht;
use App\Http\Handlers\BerichtHandler;
use App\Http\Handlers\PhotoHandler;
use Illuminate\Http\Request;

class BerichtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $berichten = Bericht::all()->load('header');


        return view('pages.pages.index',compact('berichten'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        //
        $bericht = BerichtHandler::show($title);

        return view('pages.pages.page',compact('bericht'));
    }

    public function header($id){

        $image = PhotoHandler::get($id);

        return response()->make($image, 200,['content-type' => 'image/jpg']);

    }
}
