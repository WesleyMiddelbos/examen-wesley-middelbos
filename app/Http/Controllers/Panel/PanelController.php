<?php

namespace App\Http\Controllers\Panel;

use App\Http\Handlers\BerichtHandler;
use App\Http\Handlers\GebruikerHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelController extends Controller
{
    //
    public function validateTitle(Request $request){
        return response()->json(BerichtHandler::checkTitleAvailable($request->input('title')));
    }

    public function validateEmail(Request $request){

        return response()->json(GebruikerHandler::checkEmailUnique($request->input('email')));

    }
}
