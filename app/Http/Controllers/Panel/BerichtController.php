<?php

namespace App\Http\Controllers\Panel;

use App\Bericht;
use App\Http\Handlers\BerichtHandler;
use App\Http\Handlers\PhotoHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BerichtController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $berichten = Bericht::all();

        return view('pages.pages.index',compact('berichten'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!BerichtHandler::checkTitleAvailable($request->input('titel'))) {
            $bericht = BerichtHandler::store($request->all());
            if($request->hasFile('header')){
                $photoHandler = new PhotoHandler();
                $path = $photoHandler->store($bericht, $request->file('header'));
            }
        }else{
            $old = $request->all();
            $old['error'] = 'Titel bestaat al!';
            return redirect()->back()->withInput($old);
        }

        return redirect()->route('page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        //
        $bericht = BerichtHandler::show($title);

        return view('pages.pages.page',compact('bericht'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bericht = Bericht::find($id);

        return view('panel.pages.edit',compact('bericht'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('panel.pages.create');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        //
        $bericht = Bericht::find($id);

        if($request->input('titel') != $bericht->titel){
            if(BerichtHandler::checkTitleAvailable($request->input('titel'))){
                $input = $request->all();
                $input['error'] = "Titel is al in gebruik!";
                return redirect()->back()->withInput($input);
            }
        }

        BerichtHandler::update($bericht, $request->all());

        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Bericht::find($id)->delete();

        return redirect()->route('page.index');
    }
}
