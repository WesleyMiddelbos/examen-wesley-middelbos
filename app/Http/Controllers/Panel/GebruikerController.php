<?php

namespace App\Http\Controllers\Panel;

use App\Http\Handlers\GebruikerHandler;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GebruikerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::where('id','!=',Auth::id())->get();

        return view('panel.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('panel.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(GebruikerHandler::checkEmailUnique($request->input('email'))){
            $input = $request->all();
            $input['error'] = 'Email bestaat al!';
            return redirect()->back()->withInput($input);
        }

        GebruikerHandler::store($request->all());

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);

        return view('panel.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);

        $update_user = $request->all();

        if($user->email != $update_user['email']){
            if(GebruikerHandler::checkEmailUnique($update_user['email'])){
                $update_user['error'] = "Email bestaat al!";
                return redirect()->back()->withInput($update_user);
            }
        }

        if($request->input('password') == ""){
            unset($update_user['password']);
        }else{
            $update_user['password'] = bcrypt($request->input('password'));
        }
        GebruikerHandler::update($user, $update_user);

        return redirect()->route('user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();

        return redirect()->route('user.index');
    }
}
