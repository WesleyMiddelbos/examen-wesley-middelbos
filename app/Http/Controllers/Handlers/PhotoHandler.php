<?php

namespace App\Http\Handlers;

use App\Bericht;
use App\Image;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class PhotoHandler{

    public static function get($id){

        $picture = Image::find($id);
        $image = Storage::get($picture->path);

        return $image;

    }

    public function store(Bericht $bericht, $file){
        $manager = new ImageManagerStatic();
        $img = $manager->make($file)->encode('jpg');
        $id = time().rand(0,800).".jpg";

        $path = storage_path('app\public\images/').'picture_'.$id;

        //storage path werkt niet
        $img->save($path);

        $this->recordImage($bericht, 'public\images/picture_'.$id);
    }

    private function recordImage(Bericht $bericht, $path){
        $image = new Image();
        $image->pageid = $bericht->id;
        $image->path = $path;
        $image->save();

    }

}

?>