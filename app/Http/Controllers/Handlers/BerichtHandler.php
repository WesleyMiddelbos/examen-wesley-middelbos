<?php

namespace App\Http\Handlers;

use App\Bericht;

class BerichtHandler{

    public static function store($in_bericht){

        $bericht = new Bericht();
        $bericht->fill($in_bericht);
        $bericht->save();

        return $bericht;
    }

    public static function show($title){

        $bericht = Bericht::where('titel','=',$title)->first();

        return $bericht;

    }

    public static function checkTitleAvailable($title){

        if(Bericht::where('titel','=',$title)->exists()){
            return true;
        }
        return false;

    }

    public static function update(Bericht $bericht, $update_bericht){

        $bericht->update($update_bericht);

        return $bericht;

    }

}

?>