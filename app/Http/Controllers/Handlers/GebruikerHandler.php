<?php

namespace App\Http\Handlers;

use App\User;

class GebruikerHandler{

    public static function update(User $user ,$update_user){

        $user->update($update_user);

        return $user;

    }

    public static function checkEmailUnique($email){

        return User::where('email','=',$email)->exists();

    }

    public static function store($in_user){

        $user = new User();
        $user->name = $in_user['name'];
        $user->email = $in_user['email'];
        $user->password = bcrypt($in_user['password']);
        $user->save();

        return $user;

    }

}

?>