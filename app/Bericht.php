<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bericht extends Model
{
    //
    protected $fillable = ['titel','content'];

    public function header(){
        return $this->hasOne(Image::class,'pageid','id');
    }
}
