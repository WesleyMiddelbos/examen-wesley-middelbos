<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BerichtenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('berichts')->insert([
            'titel' => 'Test',
            'content' => 'supertest',
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}
