<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GebruikersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@test.nl',
            'password' => bcrypt('test'),
            'api_token' =>  bin2hex(openssl_random_pseudo_bytes(30)),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}
